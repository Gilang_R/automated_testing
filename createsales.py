from selenium import webdriver
from jurnal_navigations import Navigations
from sales_helper import SalesInvoiceHelper
from selenium.webdriver.chrome.options import Options

chromeOptions = Options()
chromeOptions.add_argument("--kiosk")
driver = webdriver.Chrome()
#driver = webdriver.Chrome(chrome_options=chromeOptions)
n = Navigations(driver)
si = SalesInvoiceHelper(driver)

n.open_newblueranger()
n.login_aim()
# n.activate_automation_company()
n.open_sales_index()

si.create_new_salesinvoice()
si.select_customer('Costumer 1')
si.tick_shipping()
si.transaction_address('jl. auto auto no. 110')
si.tick_shipping_same_as_address()
si.ship_via('jne')
si.tracking_number('123456789')
si.set_transaction_date('01/01/2018')
si.select_tags('tag1')
si.scroll_page()
si.select_product('produk 1')
# si.tax_inclusive_toggle()
si.add_qty('10')
si.add_price('10000')
si.add_discount('10')
si.select_tax('PPN')
si.click_anywhere()
si.add_message('automated testing')
si.add_memo('automated')
si.click_create_button()
si.check_if_created()

n.view_journal_entry()
n.check_journal_entry()
n.close_browser()