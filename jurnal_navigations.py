from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *


class Navigations(object):

    def __init__(self, driver):
        self.driver = driver

    def open_ppe(self):
        self.driver.get('http://ppe-jurnal-1.ap-southeast-1.elasticbeanstalk.com')
        self.driver.maximize_window()
        print('Jurnal ppe is opened')

    def open_blueranger(self):
        self.driver.get('http://bluerangers1.ap-southeast-1.elasticbeanstalk.com/')
        self.driver.maximize_window()
        print('Blueranger is opened')

    def open_newblueranger(self):
        self.driver.get('https://blue.cd.jurnal.id/')
        self.driver.maximize_window()
        print('New Blueranger is opened')

    def open_jurnal_ap(self):
        self.driver.get('http://jurnal.ap-southeast-1.elasticbeanstalk.com/')
        self.driver.maximize_window()
        print('Jurnal ap is opened')

    def login_gara(self):
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('gara.handhito@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('64r$h4ndh1t)')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Gara')

    def login_aim(self):
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('abbad.imad@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('jurnal123')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Aim')

    def login_irene(self):    
        email_login = self.driver.find_element_by_name('user[email]')
        email_login.clear()
        email_login.send_keys('irene@jurnal.id')
        password_login = self.driver.find_element_by_name('user[password]')
        password_login.clear()
        password_login.send_keys('irene123')
        password_login.send_keys(Keys.RETURN)
        assert 'Dashboard' or 'Dasbor' in driver.title
        print('Logged in as Irene')

    def activate_automation_company(self):
        self.driver.find_element_by_css_selector('div.nav-user-text').click()
        self.driver.find_element_by_link_text('Company List').click()

        automation_company = self.driver.find_element_by_link_text('Automation')
        self.driver.execute_script("arguments[0].scrollIntoView();", automation_company)
        automation_company.click()

        print('Automation company is active')

    def open_reports_index(self):
        self.driver.find_element_by_id('vnav-reports-link').click()
        print('Reports index is opened')

    def open_cashbank_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Cash & Bank index is opened')

    def open_sales_index(self):
        self.driver.find_element_by_id('vnav-sales-link').click()
        print('Sales index is opened')

    def open_purchases_index(self):
        self.driver.find_element_by_id('vnav-purchases-link').click()
        print('Purchases index is opened')

    def open_expenses_index(self):
        try:
            WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException, WebDriverException]).until(
                ec.element_to_be_clickable((By.ID, 'vnav-expenses-link'))
            )
        finally:
            self.driver.find_element_by_id('vnav-expenses-link').click()
            print('Expenses index is opened')

    def open_bankwithdrawal_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Bank Withdrawal index is opened')

    def open_bankdeposit_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Bank Deposit index is opened')

    def open_banktransfer_index(self):
        self.driver.find_element_by_id('vnav-cashbank-link').click()
        print('Bank Transfer index is opened')

    def open_customers_index(self):
        self.driver.find_element_by_id('vnav-customers-link').click()
        print('Customers index is opened')

    def open_vendors_index(self):
        self.driver.find_element_by_id('vnav-vendors-link').click()
        print('Vendors index is opened')

    def open_products_index(self):
        self.driver.find_element_by_id('vnav-inventory-link').click()
        print('Products index is opened')

    def open_assets_index(self):
        self.driver.find_element_by_id('vnav-assetmanagement-link').click()
        print('Assets index is opened')

    def open_accounts_index(self):
        self.driver.find_element_by_id('vnav-chartofaccounts-link').click()
        print('Chart of Accounts is opened')

    def open_other_lists(self):
        self.driver.find_element_by_id('vnav-otherlists-link').click()
        print('Other lists is opened')

    def open_add_ons(self):
        self.driver.find_element_by_id('vnav-add-ons-link').click()
        print('Add-ons index is opened')

    def open_settings(self):
        self.driver.find_element_by_id('vnav-settings-link').click()
        print('Settings is opened')

    def logout_user(self):
        self.driver.find_element_by_id('vnav-logout-link').click()
        print('Gara has logged out')

    def close_browser(self):
        self.driver.quit()
        print('Browser has been closed')

#    def new_cashbank_transaction(self):
#        self.driver.find_element_by_css_selector(
#            'button.btn.btn-action-recon.dropdown-toggle.btn-dropdown-action').click()

#    def new_bank_transfer(self):`
#        self.driver.find_element_by_css_selector('a[href="/bank_transfers/new"]').click()

    def view_journal_entry(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'a.small_journal_link.get-ajax-account-transaction'))
            )
        finally:
            self.driver.find_element_by_css_selector('a.small_journal_link.get-ajax-account-transaction').click()
        # except NoSuchElementException:
        #    print('Journal entry is not found')

    def check_journal_entry(self):
        debit = self.driver.find_element_by_css_selector('td.grand-total:nth-child(3)').text
        credit = self.driver.find_element_by_css_selector('td.grand-total:nth-child(4)').text
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'a.small_journal_link.get-ajax-account-transaction'))
            )
        finally:
            self.driver.find_element_by_css_selector('a.small_journal_link.get-ajax-account-transaction').click()
        index = 0
        while ((debit != credit) or (debit == '0,00' and credit == '0,00')) and index < 2:
            self.driver.refresh()
            view_journal_entry()
            index += 1
            if debit != credit:
                print(debit)
                print(credit)
                print('Sales Invoice is not balanced')
            if debit == 0 or credit == 0:
                print('Debit or credit is 0. Please check sidekiq')
            else:
                print('Sales Invoice is balanced')
            break
