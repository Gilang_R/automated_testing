from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *

class PurchaseInvoiceHelper(object):

	def __init__(self, driver):
	    self.driver = driver

	def create_new_purchaseinvoice(self):
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.overlay-background'))
				)
		finally:
			self.driver.find_element_by_css_selector('div[class="dropdown2 inliner relative dropdown-create-transaction"]').click()
			self.driver.find_element_by_xpath('//*[@id="main-content"]/header/div/div[2]/div/div/ul/li[1]/a').click()

	def create_vendor(self, vendor):
		self.driver.find_element_by_id('s2id_transaction_person_id').click()
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.ID, 'select2-result-label-0'))
				)
		finally:
			self.driver.find_element_by_id('select2-result-label-0').click()
			try:
				WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
					ec.element_to_be_clickable((By.ID, 'person_display_name'))
					)
			finally:
				person = self.driver.find_element_by_id('person_display_name')
				person.click()
				person.clear()
				person.send_keys(vendor)
				self.driver.find_element_by_id("add-new-vendor").click()
				try:
					self.driver.find_element_by_xpath('//*[@id="vendor-form"]/div[1]')
				except NoSuchElementException:
					pass
				finally:
					print('vendor already exist')
				
	def select_vendor(self, vendor):
		self.driver.find_element_by_id('s2id_transaction_person_id').click()
		self.driver.find_element_by_id('s2id_autogen13_search').send_keys(vendor)
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
				)
		finally:
			self.driver.find_element_by_class_name('select2-result-label').click()

	def tick_shipping(self):
		self.driver.find_element_by_id('transaction_is_shipped').click()

	def transaction_address(self, address):
		alamat = self.driver.find_element_by_id('transaction_address')
		alamat.click()
		alamat.clear()
		alamat.send_keys(address)

	def tick_shipping_same_as_address(self):
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.ID, 'transaction_is_shipping_address_as_billing'))
				)
		finally:
			self.driver.find_element_by_id('transaction_is_shipping_address_as_billing').click()

	def shipping_address(self, shipping):
		ship = self.driver.find_element_by_id('transaction_shipping_address')
		ship.click()
		ship.clear()
		ship.send_keys(shipping)

	def set_transaction_date(self, date):
	    date_field = self.driver.find_element_by_id('transaction_transaction_date')
	    date_field.click()
	    date_field.clear()
	    date_field.send_keys(date)

	def ship_via(self, via):
		shipvia = self.driver.find_element_by_id('transaction_ship_via')
		shipvia.click()
		shipvia.clear()
		shipvia.send_keys(via)

	def tracking_number(self, tracking):
		tracking_field = self.driver.find_element_by_id('transaction_tracking_no')
		tracking_field.click()
		tracking_field.clear()
		tracking_field.send_keys(tracking)

	def new_transaction_number(self, prefix, postfix):
		self.driver.find_element_by_id('transaction-format-change').click()
		prefixformat = self.driver.find_element_by_id('transaction_no_sequence_prefix')
		prefixformat.click()
		prefixformat.clear()
		prefixformat.send_keys(prefix)
		postfixformat = self.driver.find_element_by_id('transaction_no_sequence_postfix')
		postfixformat.click()
		postfixformat.clear()
		postfixformat.send_keys(postfix)
		self.driver.find_element_by_id('create_transaction').click()

	def select_tags(self, tag):
		action = ActionChains(self.driver)
		self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
		tag_field = self.driver.find_element_by_id('s2id_autogen12')
		tag_field.clear()
		tag_field.send_keys(tag)
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
	        	ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
	        	)
		finally:
			element = self.driver.find_element_by_xpath('//*[@id="select2-drop"]/ul/li')
			action.move_to_element(element)
			action.click()
			action.perform()

	def select_warehouse(self, warehouse):
		self.driver.find_element_by_id('s2id_transaction_warehouse_id')
		self.driver.find_element_by_id('s2id_autogen22_search').send_keys(warehouse)
		try:
			self.driver.find_element_by_class_name('select2-result-label').click()
		except NoSuchElementException:
			print('warehouse not found')

	def select_product(self, product):
		self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_product_id').click()
		self.driver.find_element_by_id('s2id_autogen14_search').send_keys(product)
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
				)
		except NoSuchElementException:
			print('product not found')
		finally:
			self.driver.find_element_by_class_name('select2-result-label').click()

	def add_qty(self, qty):
		quantity = self.driver.find_element_by_xpath('//*[@id="transaction_transaction_lines_attributes_0_quantity"]')
		quantity.click()
		quantity.clear()
		quantity.send_keys(qty)

	def add_price(self, price):
		unitprice = self.driver.find_element_by_xpath('//*[@id="transaction_transaction_lines_attributes_0_rate"]')
		unitprice.click()
		unitprice.send_keys(price)

	def add_discount(self, discount):
		discountline = self.driver.find_element_by_id('transaction_transaction_lines_attributes_0_discount')
		discountline.click()
		discountline.clear()
		discountline.send_keys(discount)

	def select_tax(self, selecttax):
		self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_line_tax_id').click()
		self.driver.find_element_by_id('s2id_autogen16_search').send_keys(selecttax)
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
				)
		except NoSuchElementException:
			self.driver.find_element_by_class_name('select2-drop-mask').click()
			print('tax not found')
		finally:
			self.driver.find_element_by_class_name('select2-result-label').click()

	def add_tax(self, newtax, taxamount):
		self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_line_tax_id').click()
		self.driver.find_element_by_css_selector('add_new_product_link_child:nth-child(1)').click()
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.ID, 'company_tax_name'))
				)
		finally:
			taxname = self.driver.find_element_by_id('company_tax_name')
			taxname.click()
			taxname.send_keys(newtax)
			taxrate = self.driver.find_element_by_id('company_tax_rate')
			taxrate.click()
			taxrate.send_keys(taxamount)
			self.driver.find_element_by_id('add-new-company-tax').click()

	def add_witholding_tax(self, newwhtax, whtaxamount):
		self.driver.find_element_by_id('s2id_transaction_transaction_lines_attributes_0_line_tax_id').click()
		self.driver.find_element_by_css_selector('add_new_product_link_child:nth-child(1)').click()
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.ID, 'company_tax_name'))
				)
		finally:
			taxname = self.driver.find_element_by_id('company_tax_name')
			taxname.click()
			taxname.send_keys(newwhtax)
			taxrate = self.driver.find_element_by_id('company_tax_rate')
			taxrate.click()
			taxrate.send_keys(whtaxamount)
			self.driver.find_element_by_id('company_tax_is_witholding').click()
			self.driver.find_element_by_id('add-new-company-tax').click()

	def click_anywhere(self):
		self.driver.find_element_by_css_selector('div.copyright').click()

	def tax_inclusive_toggle(self):
		self.driver.find_element_by_class_name('tax-inclusive-toogle').click()	

	def scroll_page(self):
		element = self.driver.find_element_by_id('create_button')
		actions = ActionChains(self.driver)
		actions.move_to_element(element).perform()

	def add_message(self, message):
		addmessage = self.driver.find_element_by_xpath('//*[@id="transaction_message"]')
		addmessage.click()
		addmessage.send_keys(message)

	def add_memo(self, memo):
		addmemo = self.driver.find_element_by_xpath('//*[@id="transaction_memo"]')
		addmemo.click()
		addmemo.send_keys(memo)

	def click_create_button(self):
		self.driver.find_element_by_xpath('//*[@id="create_button"]').click()

	def check_if_created(self):
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.presence_of_element_located((By.XPATH, '//*[@id="page-wrapper"]/div[3]'))
				)
		except ElementNotVisibleException:
			print('Invoice is not created')
		finally:
			print('Successfully Created')


