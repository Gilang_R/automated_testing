from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *


class ExpenseHelper(object):

    def __init__(self, driver):
        self.driver = driver
        
    def create_new_expense(self):
        self.driver.implicitly_wait(2)
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'a.btn.btn-action:nth-child(1)'))
            )
        finally:

            self.driver.find_element_by_css_selector('a.btn.btn-action:nth-child(1)').click()

    def select_pay_from(self, account):
        account = account.text()
        self.driver.find_element_by_id('s2id_transaction_refund_from_id').click()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'div.add_new_product_link_child'))
            )
        finally:
            payfrom = self.driver.find_element_by_css_selector('input.select2-input:nth-child(1)')
            payfrom.click()
            payfrom.clear()
            payfrom.sendkeys(account)

    def tick_pay_later(self):
        self.driver.find_element_by_id('transaction_expense_payable').click()

    def select_beneficiary(self, beneficiary):
        self.driver.find_element_by_id('s2id_transaction_person_id').click()
        beneficiary = beneficiary.text()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'div.add_new_product_link_child'))
            )
        finally:
            input_beneficiary = self.driver.find_element_by_css_selector('div.select2-search:nth-child(4)')
            input_beneficiary.clear()
            input_beneficiary.send_keys(beneficiary)

    def set_transaction_date(self, date):
        date = date.text()
        date_field = self.driver.find_element_by_id('transaction_transaction_date')
        date_field.click()
        date_field.clear()
        date_field.send_keys(date)

    def set_payment_method(self, method):
        method = method.text()
        self.driver.find_element_by_id('s2id_transaction_payment_method_id').click()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'div.add_new_product_link_child'))
            )
        finally:
            payment_method = self.driver.find_element_by_css_selector('input.select2-input:nth-child(4)')
            payment_method.clear()
            payment_method.send_keys(method)

    def add_tags(self, tag):
        tag = tag.text()
        self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'li.select2-no-results'))
            )
        finally:
            tag_field = self.driver.find_element_by_id('s2id_transaction_tag_ids')
            tag_field.clear()
            tag_field.send_keys(tag)

    def select_expense_account(self, expense_account):
        expense_account = expense_account.text()
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_account_id').click()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'div.add_new_product_link_child'))
            )
        finally:
            expense_input = self.driver.find_element_by_css_selector('input.select2-input:nth-child(6)')
            expense_input.clear()
            expense_input.send_keys(expense_account)

    def select_tax(self, tax):
        tax = tax.text()
        self.driver.find_element_by_id('s2id_transaction_transaction_account_lines_attributes_0_line_tax_id').click()
        try:
            WebDriverWait(self.driver, 10).until(
                ec.element_to_be_clickable((By.CSS_SELECTOR, 'div.add_new_product_link_child'))
            )
        finally:
            tax_input = self.driver.find_element_by_css_selector('input.select2-input:nth-child(7)')
            tax_input.clear()
            tax_input.send_keys(tax)

    def set_amount(self, amount):
        amount = amount.text()
        amount_field = self.driver.find_element_by_id('transaction_transaction_account_lines_attributes_0_debit')
        amount_field.click()
        amount_field.send_keys(amount)
        self.driver.find_element_by_css_selector('div.copyright').click()

    def create_expense(self):
        self.driver.find_element_by_id('create_button').click()

