from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *
from select2helper import Select2Helper

class BankTransferHelper(object):

	def __init__(self, driver):
	    self.driver = driver
	    self.sh = Select2Helper(driver)
	    self.wait = WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])

	def create_new_banktransfer(self):
		try:
			self.wait.until(
				ec.invisibility_of_element_located((By.CSS_SELECTOR, 'div.overlay-background'))
				)
		finally:
			self.driver.find_element_by_class_name('btn-action-recon').click()
			self.driver.find_element_by_xpath('//*[@id="main-content"]/section/div[2]/div/div/div[1]/div[2]/div/div/ul/li[1]').click()

	def select_transferfrom(self, transferfrom):
		self.driver.find_element_by_id('s2id_transaction_refund_from_id').click()
		self.driver.find_element_by_id('s2id_autogen6_search').send_keys(transferfrom)
		self.sh.select2()

	def select_depositto(self, depositto):
		self.driver.find_element_by_id('s2id_transaction_deposit_to_id').click()
		self.driver.find_element_by_id('s2id_autogen7_search').send_keys(depositto)
		self.sh.select2()

	def add_amount(self, amount):
		addamount = self.driver.find_element_by_id('transaction_transfer_amount')
		addamount.click()
		addamount.send_keys(amount)
		
	def add_memo(self, memo):
		addmemo = self.driver.find_element_by_xpath('//*[@id="transaction_memo"]')
		addmemo.click()
		addmemo.send_keys(memo)

	def select_tags(self, tag):
		action = ActionChains(self.driver)
		self.driver.find_element_by_id('s2id_transaction_tag_ids').click()
		tag_field = self.driver.find_element_by_id('s2id_autogen3')
		tag_field.clear()
		tag_field.send_keys(tag)
		try:
			self.wait.until(
	        	ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
	        	)
		finally:
			element = self.driver.find_element_by_xpath('//*[@id="select2-drop"]/ul/li')
			action.move_to_element(element)
			action.click()
			action.perform()	

	def set_transaction_date(self, date):
	    date_field = self.driver.find_element_by_id('transaction_transaction_date')
	    date_field.click()
	    date_field.clear()
	    date_field.send_keys(date)

	def click_create_button(self):
		self.driver.find_element_by_xpath('//*[@id="create_button"]').click()






