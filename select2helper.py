from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.common.exceptions import *

class Select2Helper(object):

	def __init__(self, driver):
	    self.driver = driver

	def select2(self):
		try:
			WebDriverWait(self.driver, 10, poll_frequency=1, ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException]).until(
				ec.element_to_be_clickable((By.CLASS_NAME, 'select2-result-label'))
				)
		finally:
			self.driver.find_element_by_class_name('select2-result-label').click()